Android Architecture MVP + Clean Architecture
===================


Especificaciones
1. Consume el API en https://developers.themoviedb.org
2. La aplicación debe mostrar 3 categorías: 
	a. Popular
	b. Top Rated
	c. Upcoming
3. Debe ser posible ver los detalles de cada película o serie
4. La aplicación debe poder ser usada en modo offline usando un mecanismo de persistencia (preferiblemente SQLite)[En Proceso]
5. Debe ser posible buscar películas y mostrar los resultados agrupados por categoría
6. Escribe en un documento las capas de la aplicación (por ejemplo persistencia, UI, etc) y las clases que las componen
7. Describe la responsabilidad de cada una de las clases creadas

#####

Librerias utilizadas en el proyecto:
- [Dagger 2](http://google.github.io/dagger/)
- [RxJava and RxAndroid](https://github.com/ReactiveX/RxJava)
- [GSON](https://github.com/google/gson)
- [OkHttp](https://github.com/square/okhttp)
- [ButterKnife](https://github.com/JakeWharton/butterknife)
- [RetroLambda](https://github.com/evant/gradle-retrolambda)
- [Picasso](https://github.com/square/picasso)
- [Timber](https://github.com/JakeWharton/timber)

Requerimientos
------------

 - [Android SDK](http://developer.android.com/sdk/index.html).
 - Android [8.0 (API 26) ](http://developer.android.com/tools/revisions/platforms.html#8.0).
 - Android SDK Tools
 - Android SDK Build tools 26.0.0
 - Android Support Repository
 - Android Support library

Capas Aplicación
----------------

- di - Dependency Injection (https://bitbucket.org/xkirox/codingchallenge/src/08804b188d466e32a5d83663a530845a793a3639/app/src/main/java/co/xkirox/codingchallenge/di/?at=master).
	Descripción (https://www.genbetadev.com/paradigmas-de-programacion/que-es-la-inyeccion-de-dependencias).
- event - Event (https://bitbucket.org/xkirox/codingchallenge/src/08804b188d466e32a5d83663a530845a793a3639/app/src/main/java/co/xkirox/codingchallenge/event/?at=master).
	Descripción Clase y metodos para enviar por medio del RxBus.
- model - Model (https://bitbucket.org/xkirox/codingchallenge/src/08804b188d466e32a5d83663a530845a793a3639/app/src/main/java/co/xkirox/codingchallenge/model/?at=master).
	Descripción (https://github.com/googlesamples/android-architecture/tree/todo-mvp/).
- mvp - Model View Presenter (https://bitbucket.org/xkirox/codingchallenge/src/08804b188d466e32a5d83663a530845a793a3639/app/src/main/java/co/xkirox/codingchallenge/mvp/?at=master).
	Descripción (https://github.com/googlesamples/android-architecture/tree/todo-mvp/).
- ui - User Interface (https://bitbucket.org/xkirox/codingchallenge/src/08804b188d466e32a5d83663a530845a793a3639/app/src/main/java/co/xkirox/codingchallenge/ui/?at=master).
	Descripción (https://github.com/googlesamples/android-architecture/tree/todo-mvp/).
- util - Utilities (https://bitbucket.org/xkirox/codingchallenge/src/08804b188d466e32a5d83663a530845a793a3639/app/src/main/java/co/xkirox/codingchallenge/util/?at=master).
	Descripción: Configurar librerias necesarias para la ejecucion de la App.
- MovieApp - App Init (https://bitbucket.org/xkirox/codingchallenge/src/08804b188d466e32a5d83663a530845a793a3639/app/src/main/java/co/xkirox/codingchallenge/MovieApp.java?at=master&fileviewer=file-view-default).
	Descripción: El método para iniciar la app.
- res - Resource (https://bitbucket.org/xkirox/codingchallenge/src/08804b188d466e32a5d83663a530845a793a3639/app/src/main/res/?at=master).
	Descripción (https://developer.android.com/reference/android/content/res/package-summary.html).



