package co.xkirox.codingchallenge.util;

import co.xkirox.codingchallenge.BuildConfig;

/**
 * Created by xkiRox on 2/08/17.
 */

public class Constants {
    public static final String API_KEY = BuildConfig.API_KEY;

    public static final String BASE_URL = "https://api.themoviedb.org/3/";
    public static final String IMAGE_BASE_URL = "https://image.tmdb.org/t/p/";

    public static final String IMAGE_W185 = "w185";
    public static final String IMAGE_W342 = "w342";
    public static final String IMAGE_W780 = "w780";

}
