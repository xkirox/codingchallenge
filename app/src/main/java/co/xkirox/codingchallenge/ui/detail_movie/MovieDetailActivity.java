package co.xkirox.codingchallenge.ui.detail_movie;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import co.xkirox.codingchallenge.MovieApp;
import co.xkirox.codingchallenge.R;
import co.xkirox.codingchallenge.di.detailmovie.DaggerDetailMovieComponent;
import co.xkirox.codingchallenge.di.detailmovie.DetailMovieModule;
import co.xkirox.codingchallenge.event.PopularDetailEvent;
import co.xkirox.codingchallenge.event.SearchDetailEvent;
import co.xkirox.codingchallenge.event.TopRatedDetailEvent;
import co.xkirox.codingchallenge.event.UpcomingDetailEvent;
import co.xkirox.codingchallenge.util.Constants;
import co.xkirox.codingchallenge.util.RxBus;
import rx.subscriptions.CompositeSubscription;
import timber.log.Timber;

public class MovieDetailActivity extends AppCompatActivity {

    @Inject RxBus bus;

    @BindView(R.id.coverImage) ImageView coverImage;
    @BindView(R.id.detail_title) TextView detailTitle;
    @BindView(R.id.detail_backdrop) ImageView detailBackdrop;
    @BindView(R.id.release_date) TextView releaseDate;
    @BindView(R.id.detail_overview) TextView detailOverview;

    private CompositeSubscription mSubscription = new CompositeSubscription();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_movie_detail);

        ButterKnife.bind(this);

        initInjector();

        mSubscription.add(bus.toStickyObservable()
                .subscribe(event -> {
                            if (event instanceof PopularDetailEvent) {
                                detailTitle.setText(((PopularDetailEvent) event).getResults().getTitle());
                                releaseDate.setText(((PopularDetailEvent) event).getResults().getRelease_date());
                                detailOverview.setText(((PopularDetailEvent) event).getResults().getOverview());

                                Picasso.with(this)
                                        .load(Constants.IMAGE_BASE_URL + Constants.IMAGE_W342 + ((PopularDetailEvent) event).getResults().getPoster_path())
                                        .placeholder(R.mipmap.ic_movie)
                                        .into(detailBackdrop);

                                Picasso.with(this)
                                        .load(Constants.IMAGE_BASE_URL + Constants.IMAGE_W780 + ((PopularDetailEvent) event).getResults().getPoster_path())
                                        .into(coverImage);

                            } else if (event instanceof TopRatedDetailEvent) {
                                detailTitle.setText(((TopRatedDetailEvent) event).getResults().getTitle());
                                releaseDate.setText(((TopRatedDetailEvent) event).getResults().getRelease_date());
                                detailOverview.setText(((TopRatedDetailEvent) event).getResults().getOverview());

                                Picasso.with(this)
                                        .load(Constants.IMAGE_BASE_URL + Constants.IMAGE_W342 + ((TopRatedDetailEvent) event).getResults().getPoster_path())
                                        .placeholder(R.mipmap.ic_movie)
                                        .into(detailBackdrop);

                                Picasso.with(this)
                                        .load(Constants.IMAGE_BASE_URL + Constants.IMAGE_W780 + ((TopRatedDetailEvent) event).getResults().getPoster_path())
                                        .into(coverImage);
                            } else if (event instanceof UpcomingDetailEvent) {
                                detailTitle.setText(((UpcomingDetailEvent) event).getResults().getTitle());
                                releaseDate.setText(((UpcomingDetailEvent) event).getResults().getReleaseDate());
                                detailOverview.setText(((UpcomingDetailEvent) event).getResults().getOverview());

                                Picasso.with(this)
                                        .load(Constants.IMAGE_BASE_URL + Constants.IMAGE_W342 + ((UpcomingDetailEvent) event).getResults().getPosterPath())
                                        .placeholder(R.mipmap.ic_movie)
                                        .into(detailBackdrop);

                                Picasso.with(this)
                                        .load(Constants.IMAGE_BASE_URL + Constants.IMAGE_W780 + ((UpcomingDetailEvent) event).getResults().getPosterPath())
                                        .into(coverImage);
                            }else if (event instanceof SearchDetailEvent) {
                                detailTitle.setText(((SearchDetailEvent) event).getResults().getTitle());
                                releaseDate.setText(((SearchDetailEvent) event).getResults().getRelease_date());
                                detailOverview.setText(((SearchDetailEvent) event).getResults().getOverview());

                                Picasso.with(this)
                                        .load(Constants.IMAGE_BASE_URL + Constants.IMAGE_W342 + ((SearchDetailEvent) event).getResults().getPoster_path())
                                        .placeholder(R.mipmap.ic_movie)
                                        .into(detailBackdrop);

                                Picasso.with(this)
                                        .load(Constants.IMAGE_BASE_URL + Constants.IMAGE_W780 + ((SearchDetailEvent) event).getResults().getPoster_path())
                                        .into(coverImage);
                            }

                        },
                        e -> Timber.e(e.getMessage())));
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if(mSubscription != null && mSubscription.isUnsubscribed())
            mSubscription.unsubscribe();
    }

    private void initInjector() {
        DaggerDetailMovieComponent.builder()
                .appComponent(MovieApp.get(this).getAppComponent())
                .detailMovieModule(new DetailMovieModule())
                .build().inject(this);
    }
}
