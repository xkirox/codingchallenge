package co.xkirox.codingchallenge.ui.toprated;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;

import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import co.xkirox.codingchallenge.MovieApp;
import co.xkirox.codingchallenge.R;
import co.xkirox.codingchallenge.di.toprated.DaggerTopRatedComponent;
import co.xkirox.codingchallenge.di.toprated.TopRatedModule;
import co.xkirox.codingchallenge.model.entity.toprated.TopRatedResults;
import co.xkirox.codingchallenge.mvp.presenter.toprated.TopRatedPresenter;
import co.xkirox.codingchallenge.mvp.view.toprated.ITopRatedView;
import co.xkirox.codingchallenge.util.RxBus;

public class TopRatedFragment extends Fragment implements ITopRatedView {

    @BindView(R.id.topRated_recyclerView) RecyclerView recyclerView;
    @BindView(R.id.topRatedProgress) ProgressBar progressBar;

    @Inject
    TopRatedPresenter presenter;
    @Inject
    RxBus bus;

    TopRatedRecyclerViewAdapter adapter;

    public TopRatedFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_top_rated, container, false);

        ButterKnife.bind(this,view);

        initInjector();

        presenter.loadTopRatedMovies();

        return view;
    }

    @Override
    public void showProgress() {
        progressBar.setVisibility(View.VISIBLE);
        recyclerView.setVisibility(View.GONE);
    }

    @Override
    public void hideProgress() {
        progressBar.setVisibility(View.GONE);
        recyclerView.setVisibility(View.VISIBLE);
    }

    @Override
    public void showTopRatedMovies(List<TopRatedResults> topRatedResultsList) {
        adapter = new TopRatedRecyclerViewAdapter(topRatedResultsList, getContext(),bus);
        adapter.notifyDataSetChanged();
        recyclerView.setAdapter(adapter);
        recyclerView.setLayoutManager(new GridLayoutManager(getContext(),3));
    }

    private void initInjector() {
        DaggerTopRatedComponent.builder()
                .appComponent(MovieApp.get(getContext()).getAppComponent())
                .topRatedModule(new TopRatedModule(this))
                .build().inject(this);
    }
}
