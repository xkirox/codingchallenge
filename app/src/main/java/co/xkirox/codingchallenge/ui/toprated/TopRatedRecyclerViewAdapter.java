package co.xkirox.codingchallenge.ui.toprated;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import co.xkirox.codingchallenge.R;
import co.xkirox.codingchallenge.event.TopRatedDetailEvent;
import co.xkirox.codingchallenge.model.entity.toprated.TopRatedResults;
import co.xkirox.codingchallenge.ui.detail_movie.MovieDetailActivity;
import co.xkirox.codingchallenge.util.Constants;
import co.xkirox.codingchallenge.util.RxBus;

/**
 * Created by xkiRox on 4/08/17.
 */

public class TopRatedRecyclerViewAdapter extends RecyclerView.Adapter<TopRatedRecyclerViewAdapter.ViewHolder> {

    private List<TopRatedResults> topRatedResultsList;
    private Context context;
    private RxBus bus;

    @Inject
    public TopRatedRecyclerViewAdapter(List<TopRatedResults> topRatedResultsList, Context context, RxBus bus) {
        this.topRatedResultsList = topRatedResultsList;
        this.context = context;
        this.bus = bus;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_movie, parent, false);

        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        TopRatedResults results = topRatedResultsList.get(position);

        Picasso.with(context)
                .load(Constants.IMAGE_BASE_URL + Constants.IMAGE_W185 + results.getPoster_path())
                .placeholder(R.mipmap.ic_movie)
                .into(holder.poster);

        holder.poster.setOnClickListener(v -> {
            bus.postSticky(new TopRatedDetailEvent(results));
            context.startActivity(new Intent(context, MovieDetailActivity.class));
        });
    }

    @Override
    public int getItemCount() {
        return topRatedResultsList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.moviePoster)
        ImageView poster;

        public ViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }
    }
}
