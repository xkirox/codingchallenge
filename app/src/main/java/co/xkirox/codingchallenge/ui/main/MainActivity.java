package co.xkirox.codingchallenge.ui.main;

import android.content.Intent;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.SearchView;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import co.xkirox.codingchallenge.MovieApp;
import co.xkirox.codingchallenge.R;
import co.xkirox.codingchallenge.di.search.DaggerSearchMovieComponent;
import co.xkirox.codingchallenge.di.search.SearchMovieModule;
import co.xkirox.codingchallenge.event.SearchDetailEvent;
import co.xkirox.codingchallenge.model.entity.search.SearchResults;
import co.xkirox.codingchallenge.mvp.presenter.search.SearchPresenter;
import co.xkirox.codingchallenge.mvp.view.search.ISearchView;
import co.xkirox.codingchallenge.ui.detail_movie.MovieDetailActivity;
import co.xkirox.codingchallenge.ui.popular.PopularFragment;
import co.xkirox.codingchallenge.ui.toprated.TopRatedFragment;
import co.xkirox.codingchallenge.ui.upcoming.UpcomingFragment;
import co.xkirox.codingchallenge.util.RxBus;

public class MainActivity extends AppCompatActivity implements ISearchView{

    public static final int SEARCH_QUERY_THRESHOLD = 3;

    @BindView(R.id.mainToolbar) Toolbar toolbar;
    @BindView(R.id.tabs) TabLayout tabLayout;
    @BindView(R.id.viewPager) ViewPager viewPager;
    @BindView(R.id.list_result) ListView listView;

    @Inject SearchPresenter presenter;

    SearchView searchView;
    ArrayAdapter<String> adapter;
    private List<SearchResults> list;

    @Inject RxBus rxBus;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ButterKnife.bind(this);

        initInjector();

        setSupportActionBar(toolbar);
        getSupportActionBar().setHomeButtonEnabled(true);

        listView.setOnItemClickListener((adapterView, view, i, l) -> {
            rxBus.postSticky(new SearchDetailEvent(list.get(i)));
            this.startActivity(new Intent(this, MovieDetailActivity.class));
        });

        setupViewPager(viewPager);
        tabLayout.setupWithViewPager(viewPager);
    }

    private void setupViewPager(ViewPager viewPager) {
        ViewPagerAdapter adapter = new ViewPagerAdapter(getSupportFragmentManager());
        adapter.addFragment(new PopularFragment(), "Popular");
        adapter.addFragment(new TopRatedFragment(), "Top Rated");
        adapter.addFragment(new UpcomingFragment(), "Upcoming");
        viewPager.setAdapter(adapter);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_search, menu);

        MenuItem item = menu.findItem(R.id.menu_search);

        searchView = (SearchView) item.getActionView();

        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String s) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String s) {

                if (s.length() >= SEARCH_QUERY_THRESHOLD) {
                    presenter.searchMovies(s);
                }else{
                    listView.setVisibility(View.GONE);
                }

                return false;
            }
        });

        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void showSearchMovies(List<SearchResults> searchResultsList) {
        this.list=searchResultsList;

        ArrayList<String> title = new ArrayList<String>();

        for (SearchResults _search: searchResultsList
             ) {
            title.add(_search.getTitle());
        }

        adapter = new ArrayAdapter<String>(this,
                android.R.layout.simple_dropdown_item_1line, title);

        listView.setAdapter(adapter);

        listView.setVisibility(View.VISIBLE);

    }

    private void initInjector() {
        DaggerSearchMovieComponent.builder()
                .appComponent(MovieApp.get(this).getAppComponent())
                .searchMovieModule(new SearchMovieModule(this))
                .build().inject(this);
    }
}
