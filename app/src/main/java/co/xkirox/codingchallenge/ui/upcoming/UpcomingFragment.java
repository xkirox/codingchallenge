package co.xkirox.codingchallenge.ui.upcoming;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;

import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import co.xkirox.codingchallenge.MovieApp;
import co.xkirox.codingchallenge.R;
import co.xkirox.codingchallenge.di.upcoming.DaggerUpcomingComponent;
import co.xkirox.codingchallenge.di.upcoming.UpcomingModule;
import co.xkirox.codingchallenge.model.entity.upcoming.UpcomingResults;
import co.xkirox.codingchallenge.mvp.presenter.upcoming.UpcomingPresenter;
import co.xkirox.codingchallenge.mvp.view.upcoming.IUpcomingView;
import co.xkirox.codingchallenge.util.RxBus;

public class UpcomingFragment extends Fragment implements IUpcomingView{

    @BindView(R.id.upcoming_recyclerView) RecyclerView recyclerView;
    @BindView(R.id.upcomingProgress) ProgressBar progressBar;

    @Inject
    UpcomingPresenter presenter;
    @Inject
    RxBus bus;

    UpcomingRecyclerViewAdapter adapter;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_upcoming, container, false);


        ButterKnife.bind(this,view);

        initInjector();

        presenter.loadUpcomingMovies();

        return view;
    }

    @Override
    public void showProgress() {
        progressBar.setVisibility(View.VISIBLE);
        recyclerView.setVisibility(View.GONE);
    }

    @Override
    public void hideProgress() {
        progressBar.setVisibility(View.GONE);
        recyclerView.setVisibility(View.VISIBLE);
    }

    @Override
    public void showUpcomingMovies(List<UpcomingResults> upcomingResultsList) {
        adapter = new UpcomingRecyclerViewAdapter(upcomingResultsList, getContext(), bus);
        adapter.notifyDataSetChanged();
        recyclerView.setAdapter(adapter);
        recyclerView.setLayoutManager(new GridLayoutManager(getContext(),3));
    }

    private void initInjector() {
        DaggerUpcomingComponent.builder()
                .appComponent(MovieApp.get(getContext()).getAppComponent())
                .upcomingModule(new UpcomingModule(this))
                .build().inject(this);
    }
}
