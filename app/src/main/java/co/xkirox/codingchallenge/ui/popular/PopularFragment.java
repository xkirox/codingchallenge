package co.xkirox.codingchallenge.ui.popular;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;

import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import co.xkirox.codingchallenge.MovieApp;
import co.xkirox.codingchallenge.R;
import co.xkirox.codingchallenge.di.popular.DaggerPopularComponent;
import co.xkirox.codingchallenge.di.popular.PopularModule;
import co.xkirox.codingchallenge.model.entity.popular.PopularResults;
import co.xkirox.codingchallenge.mvp.presenter.popular.PopularPresenter;
import co.xkirox.codingchallenge.mvp.view.popular.IPopularView;
import co.xkirox.codingchallenge.util.RxBus;

public class PopularFragment extends Fragment implements IPopularView {

    @BindView(R.id.popular_recyclerView) RecyclerView recyclerView;
    @BindView(R.id.popularProgress) ProgressBar progressBar;

    @Inject PopularPresenter presenter;
    @Inject RxBus bus;

    PopularRecyclerViewAdapter adapter;

    public PopularFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_popular, container, false);
        ButterKnife.bind(this, view);

        initInjector();

        presenter.loadPopularMovies();

        return view;
    }

    @Override
    public void showProgress() {
        progressBar.setVisibility(View.VISIBLE);
        recyclerView.setVisibility(View.GONE);
    }

    @Override
    public void hideProgress() {
        progressBar.setVisibility(View.GONE);
        recyclerView.setVisibility(View.VISIBLE);
    }

    @Override
    public void showPopularMovies(List<PopularResults> popularResultsList) {
        adapter = new PopularRecyclerViewAdapter(popularResultsList, getContext(),bus);
        adapter.notifyDataSetChanged();
        recyclerView.setAdapter(adapter);
        recyclerView.setLayoutManager(new GridLayoutManager(getContext(),3));
    }

    private void initInjector() {
        DaggerPopularComponent.builder()
                .appComponent(MovieApp.get(this.getContext()).getAppComponent())
                .popularModule(new PopularModule(this))
                .build().inject(this);
    }

}
