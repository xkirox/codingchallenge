package co.xkirox.codingchallenge.model.api;

import co.xkirox.codingchallenge.model.entity.popular.PopularResponse;
import co.xkirox.codingchallenge.model.entity.search.SearchResponse;
import co.xkirox.codingchallenge.model.entity.toprated.TopRatedResponse;
import co.xkirox.codingchallenge.model.entity.upcoming.UpcomingResponse;
import retrofit2.http.GET;
import retrofit2.http.Query;
import rx.Observable;

/**
 * Created by xkiRox on 2/08/17.
 */

public interface ApiSource {

    // Popular Movies
    @GET("movie/popular")
    Observable<PopularResponse> getPopularMovies(@Query("api_key") String apiKey);

    // Top rated Movies
    @GET("movie/top_rated")
    Observable<TopRatedResponse> getTopRatedMovies(@Query("api_key") String apiKey);

    // Upcoming
    @GET("movie/upcoming")
    Observable<UpcomingResponse> getUpcomingMovies(@Query("api_key") String apiKey);

    // Search
    @GET("search/movie")
    Observable<SearchResponse> getSearchMovie(@Query("api_key") String apiKey, @Query("query") String query);

}