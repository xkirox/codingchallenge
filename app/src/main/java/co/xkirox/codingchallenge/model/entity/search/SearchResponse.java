package co.xkirox.codingchallenge.model.entity.search;

import com.google.gson.annotations.SerializedName;

import java.util.List;

import co.xkirox.codingchallenge.model.entity.toprated.TopRatedResults;

/**
 * Created by xkiRox on 7/08/17.
 */

public class SearchResponse {
    @SerializedName("page")
    private int page;

    @SerializedName("results")
    private List<SearchResults> searchResultsList;

    public int getPage() {
        return page;
    }

    public void setPage(int page) {
        this.page = page;
    }

    public List<SearchResults> getSearchResultsList() {
        return searchResultsList;
    }

    public void setSearchResultsList(List<SearchResults> searchResultsList) {
        this.searchResultsList = searchResultsList;
    }
}
