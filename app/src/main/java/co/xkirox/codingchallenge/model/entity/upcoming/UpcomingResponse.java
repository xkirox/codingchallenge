package co.xkirox.codingchallenge.model.entity.upcoming;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.Date;
import java.util.List;

/**
 * Created by xkiRox on 4/08/17.
 */

public class UpcomingResponse {
    @SerializedName("results")
    @Expose
    private List<UpcomingResults> results = null;

    @SerializedName("page")
    @Expose
    private Integer page;

    public List<UpcomingResults> getResultsList() {
        return results;
    }

    public void setResults(List<UpcomingResults> results) {
        this.results = results;
    }

    public Integer getPage() {
        return page;
    }

    public void setPage(Integer page) {
        this.page = page;
    }
}
