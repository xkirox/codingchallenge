package co.xkirox.codingchallenge.event;

import co.xkirox.codingchallenge.model.entity.search.SearchResults;

/**
 * Created by xkiRox on 7/08/17.
 */

public class SearchDetailEvent {
    SearchResults results;

    public SearchDetailEvent(SearchResults results) {
        this.results = results;
    }

    public SearchResults getResults() {
        return results;
    }

    public void setResults(SearchResults results) {
        this.results = results;
    }
}
