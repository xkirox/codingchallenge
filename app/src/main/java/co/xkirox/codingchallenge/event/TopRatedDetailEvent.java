package co.xkirox.codingchallenge.event;

import co.xkirox.codingchallenge.model.entity.toprated.TopRatedResults;

/**
 * Created by xkiRox on 4/08/17.
 */

public class TopRatedDetailEvent {
    TopRatedResults results;

    public TopRatedDetailEvent(TopRatedResults results) {
        this.results = results;
    }

    public TopRatedResults getResults() {
        return results;
    }

    public void setResults(TopRatedResults results) {
        this.results = results;
    }
}
