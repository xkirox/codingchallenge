package co.xkirox.codingchallenge.event;

import co.xkirox.codingchallenge.model.entity.popular.PopularResults;

/**
 * Created by xkiRox on 4/08/17.
 */

public class PopularDetailEvent {
    PopularResults results;

    public PopularDetailEvent(PopularResults results) {
        this.results = results;
    }

    public PopularResults getResults() {
        return results;
    }

    public void setResults(PopularResults results) {
        this.results = results;
    }
}
