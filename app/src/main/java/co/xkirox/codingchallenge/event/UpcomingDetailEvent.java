package co.xkirox.codingchallenge.event;

import co.xkirox.codingchallenge.model.entity.upcoming.UpcomingResults;

/**
 * Created by xkiRox on 5/08/17.
 */

public class UpcomingDetailEvent {
    UpcomingResults results;

    public UpcomingDetailEvent(UpcomingResults results) {
        this.results = results;
    }

    public UpcomingResults getResults() {
        return results;
    }

    public void setResults(UpcomingResults results) {
        this.results = results;
    }
}
