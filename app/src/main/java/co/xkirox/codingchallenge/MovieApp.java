package co.xkirox.codingchallenge;

import android.app.Application;
import android.content.Context;

import co.xkirox.codingchallenge.di.app.AppComponent;
import co.xkirox.codingchallenge.di.app.AppModule;
import co.xkirox.codingchallenge.di.app.DaggerAppComponent;
import co.xkirox.codingchallenge.di.app.NetworkModule;
import timber.log.Timber;

/**
 * Created by xkiRox on 2/08/17.
 */

public class MovieApp extends Application {

    AppComponent appComponent;

    @Override
    public void onCreate() {
        super.onCreate();

        initInjector();

        if (BuildConfig.DEBUG) {
            Timber.plant(new Timber.DebugTree());
        }
    }

    private void initInjector() {
        appComponent = DaggerAppComponent.builder()
                .appModule(new AppModule(this))
                .networkModule(new NetworkModule())
                .build();
    }

    public static MovieApp get(Context context) {
        return (MovieApp) context.getApplicationContext();
    }

    public AppComponent getAppComponent() {
        return appComponent;
    }
}