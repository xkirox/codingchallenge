package co.xkirox.codingchallenge.mvp.view.upcoming;

import java.util.List;

import co.xkirox.codingchallenge.model.entity.toprated.TopRatedResults;
import co.xkirox.codingchallenge.model.entity.upcoming.UpcomingResults;

/**
 * Created by xkiRox on 4/08/17.
 */

public interface IUpcomingView {

    void showProgress();

    void hideProgress();

    void showUpcomingMovies(List<UpcomingResults> upcomingResultsList);
}
