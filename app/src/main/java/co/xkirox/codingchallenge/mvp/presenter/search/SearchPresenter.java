package co.xkirox.codingchallenge.mvp.presenter.search;

import javax.inject.Inject;

import co.xkirox.codingchallenge.model.api.ApiSource;
import co.xkirox.codingchallenge.mvp.view.search.ISearchView;
import co.xkirox.codingchallenge.util.Constants;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;
import timber.log.Timber;

/**
 * Created by xkiRox on 7/08/17.
 */

public class SearchPresenter implements ISearchPresenter {

    private ISearchView view;
    private ApiSource apiSource;

    @Inject
    public SearchPresenter(ISearchView view, ApiSource apiSource) {
        this.view = view;
        this.apiSource = apiSource;
    }

    @Override
    public void searchMovies(String query) {

        apiSource.getSearchMovie(Constants.API_KEY, query)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(searchResponse -> {
                            view.showSearchMovies(searchResponse.getSearchResultsList());
                        },
                        e -> Timber.e(e.getMessage()));

    }
}
