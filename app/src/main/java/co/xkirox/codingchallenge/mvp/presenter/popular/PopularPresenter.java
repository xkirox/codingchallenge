package co.xkirox.codingchallenge.mvp.presenter.popular;

import javax.inject.Inject;

import co.xkirox.codingchallenge.model.api.ApiSource;
import co.xkirox.codingchallenge.mvp.view.popular.IPopularView;
import co.xkirox.codingchallenge.util.Constants;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;
import timber.log.Timber;

/**
 * Created by xkiRox on 4/08/17.
 */

public class PopularPresenter implements IPopularPresenter {

    private IPopularView view;
    private ApiSource apiSource;

    @Inject
    public PopularPresenter(IPopularView view, ApiSource apiSource) {
        this.view = view;
        this.apiSource = apiSource;
    }

    @Override
    public void loadPopularMovies() {
        view.showProgress();

        apiSource.getPopularMovies(Constants.API_KEY)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(popularResponse -> {
                    view.hideProgress();
                    view.showPopularMovies(popularResponse.getPopularResultsList());
                },
                e -> Timber.e(e.getMessage()));

    }
}
