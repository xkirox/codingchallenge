package co.xkirox.codingchallenge.mvp.view.toprated;

import java.util.List;

import co.xkirox.codingchallenge.model.entity.toprated.TopRatedResults;

/**
 * Created by xkiRox on 4/08/17.
 */

public interface ITopRatedView {

    void showProgress();

    void hideProgress();

    void showTopRatedMovies(List<TopRatedResults> topRatedResultsList);
}
