package co.xkirox.codingchallenge.mvp.view.search;

import java.util.List;

import co.xkirox.codingchallenge.model.entity.search.SearchResults;

/**
 * Created by xkiRox on 7/08/17.
 */

public interface ISearchView {

    void showSearchMovies(List<SearchResults> searchResultsList);

}
