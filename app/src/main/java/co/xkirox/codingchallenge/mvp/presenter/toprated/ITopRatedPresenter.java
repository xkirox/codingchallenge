package co.xkirox.codingchallenge.mvp.presenter.toprated;

/**
 * Created by xkiRox on 4/08/17.
 */

public interface ITopRatedPresenter {
    void loadTopRatedMovies();
}
