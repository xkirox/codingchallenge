package co.xkirox.codingchallenge.mvp.presenter.toprated;

import javax.inject.Inject;

import co.xkirox.codingchallenge.model.api.ApiSource;
import co.xkirox.codingchallenge.mvp.view.toprated.ITopRatedView;
import co.xkirox.codingchallenge.util.Constants;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;
import timber.log.Timber;

/**
 * Created by xkiRox on 4/08/17.
 */

public class TopRatedPresenter implements ITopRatedPresenter {
    private ITopRatedView view;
    private ApiSource apiSource;

    @Inject
    public TopRatedPresenter(ITopRatedView view, ApiSource apiSource) {
        this.view = view;
        this.apiSource = apiSource;
    }

    @Override
    public void loadTopRatedMovies() {
        view.showProgress();

        apiSource.getTopRatedMovies(Constants.API_KEY)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(topRatedResponse -> {
                            view.hideProgress();
                            view.showTopRatedMovies(topRatedResponse.getTopRatedResultsList());
                        },
                        e -> Timber.e(e.getMessage()));
    }
}
