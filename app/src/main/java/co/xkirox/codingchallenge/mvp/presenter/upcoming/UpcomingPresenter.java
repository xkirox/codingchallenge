package co.xkirox.codingchallenge.mvp.presenter.upcoming;

import javax.inject.Inject;

import co.xkirox.codingchallenge.model.api.ApiSource;
import co.xkirox.codingchallenge.mvp.view.upcoming.IUpcomingView;
import co.xkirox.codingchallenge.util.Constants;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;
import timber.log.Timber;

/**
 * Created by xkiRox on 4/08/17.
 */

public class UpcomingPresenter implements IUpcomingPresenter {

    private IUpcomingView view;
    private ApiSource apiSource;

    @Inject
    public UpcomingPresenter(IUpcomingView view, ApiSource apiSource) {
        this.view = view;
        this.apiSource = apiSource;
    }

    @Override
    public void loadUpcomingMovies() {
        view.showProgress();

        apiSource.getUpcomingMovies(Constants.API_KEY)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(upcomingResponse -> {
                            view.hideProgress();
                            view.showUpcomingMovies(upcomingResponse.getResultsList());
                        },
                        e -> Timber.e(e.getMessage()));
    }
}
