package co.xkirox.codingchallenge.mvp.presenter.upcoming;

/**
 * Created by xkiRox on 4/08/17.
 */

public interface IUpcomingPresenter {
    void loadUpcomingMovies();
}
