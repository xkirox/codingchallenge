package co.xkirox.codingchallenge.mvp.presenter.search;

/**
 * Created by xkiRox on 7/08/17.
 */

public interface ISearchPresenter {

    void searchMovies(String search);
}
