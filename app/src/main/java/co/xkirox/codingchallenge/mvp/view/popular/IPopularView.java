package co.xkirox.codingchallenge.mvp.view.popular;

import java.util.List;

import co.xkirox.codingchallenge.model.entity.popular.PopularResults;

/**
 * Created by xkiRox on 4/08/17.
 */

public interface IPopularView {

    void showProgress();

    void hideProgress();

    void showPopularMovies(List<PopularResults> popularResultsList);
}
