package co.xkirox.codingchallenge.mvp.presenter.popular;

/**
 * Created by xkiRox on 4/08/17.
 */

public interface IPopularPresenter {

    void loadPopularMovies();

}
