package co.xkirox.codingchallenge.di.detailmovie;

import co.xkirox.codingchallenge.di.PerActivity;
import co.xkirox.codingchallenge.di.app.AppComponent;
import co.xkirox.codingchallenge.ui.detail_movie.MovieDetailActivity;
import dagger.Component;

/**
 * Created by xkiRox on 5/08/17.
 */

@PerActivity
@Component(modules = DetailMovieModule.class, dependencies = AppComponent.class)
public interface DetailMovieComponent {
    void inject(MovieDetailActivity activity);
}
