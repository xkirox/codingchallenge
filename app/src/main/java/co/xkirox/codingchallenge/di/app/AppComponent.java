package co.xkirox.codingchallenge.di.app;

import co.xkirox.codingchallenge.model.api.ApiSource;
import co.xkirox.codingchallenge.util.RxBus;

import javax.inject.Singleton;

import dagger.Component;

/**
 * Created by xkiRox on 2/08/17.
 */

@Singleton
@Component(modules = {AppModule.class, NetworkModule.class})
public interface AppComponent {
    ApiSource apiSource();
    RxBus rxbus();
}
