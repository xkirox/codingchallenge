package co.xkirox.codingchallenge.di.popular;

import co.xkirox.codingchallenge.MovieApp;
import co.xkirox.codingchallenge.di.PerFragment;
import co.xkirox.codingchallenge.di.app.AppComponent;
import co.xkirox.codingchallenge.ui.main.MainActivity;
import co.xkirox.codingchallenge.ui.popular.PopularFragment;
import dagger.Component;

/**
 * Created by xkiRox on 4/08/17.
 */

@PerFragment
@Component(dependencies = AppComponent.class, modules = PopularModule.class)
public interface PopularComponent {
    void inject(PopularFragment fragment);
}
