package co.xkirox.codingchallenge.di.search;

import co.xkirox.codingchallenge.di.PerActivity;
import co.xkirox.codingchallenge.di.app.AppComponent;
import co.xkirox.codingchallenge.ui.main.MainActivity;
import dagger.Component;

/**
 * Created by xkiRox on 7/08/17.
 */

@PerActivity
@Component(modules = SearchMovieModule.class, dependencies = AppComponent.class)
public interface SearchMovieComponent {
    void inject(MainActivity activity);
}
