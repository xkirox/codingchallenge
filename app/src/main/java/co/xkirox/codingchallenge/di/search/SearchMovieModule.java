package co.xkirox.codingchallenge.di.search;

import co.xkirox.codingchallenge.di.PerActivity;
import co.xkirox.codingchallenge.di.PerFragment;
import co.xkirox.codingchallenge.model.api.ApiSource;
import co.xkirox.codingchallenge.mvp.presenter.popular.PopularPresenter;
import co.xkirox.codingchallenge.mvp.presenter.search.SearchPresenter;
import co.xkirox.codingchallenge.mvp.view.search.ISearchView;
import co.xkirox.codingchallenge.ui.main.MainActivity;
import co.xkirox.codingchallenge.ui.popular.PopularFragment;
import dagger.Module;
import dagger.Provides;

/**
 * Created by xkiRox on 7/08/17.
 */


@Module
public class SearchMovieModule {

    ISearchView view;
    MainActivity activity;

    public SearchMovieModule(ISearchView view) {
        this.view = view;
    }

    @Provides
    @PerActivity
    MainActivity provideFragment() {
        return activity;
    }
    @Provides
    @PerActivity
    SearchPresenter provideMainPresenter(ApiSource apiSource) {
        return new SearchPresenter(view,apiSource);
    }
}
