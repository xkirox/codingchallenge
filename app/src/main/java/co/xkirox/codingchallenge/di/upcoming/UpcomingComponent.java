package co.xkirox.codingchallenge.di.upcoming;

import co.xkirox.codingchallenge.di.PerFragment;
import co.xkirox.codingchallenge.di.app.AppComponent;
import co.xkirox.codingchallenge.di.toprated.TopRatedModule;
import co.xkirox.codingchallenge.ui.upcoming.UpcomingFragment;
import dagger.Component;

/**
 * Created by xkiRox on 4/08/17.
 */

@PerFragment
@Component(dependencies = AppComponent.class, modules = UpcomingModule.class)
public interface UpcomingComponent {
    void inject(UpcomingFragment fragment);
}
