package co.xkirox.codingchallenge.di.upcoming;

import co.xkirox.codingchallenge.di.PerFragment;
import co.xkirox.codingchallenge.model.api.ApiSource;
import co.xkirox.codingchallenge.mvp.presenter.upcoming.UpcomingPresenter;
import co.xkirox.codingchallenge.mvp.view.upcoming.IUpcomingView;
import co.xkirox.codingchallenge.ui.upcoming.UpcomingFragment;
import dagger.Module;
import dagger.Provides;

/**
 * Created by xkiRox on 4/08/17.
 */

@Module
public class UpcomingModule {

    IUpcomingView upcomingView;
    UpcomingFragment upcomingFragment;

    public UpcomingModule(IUpcomingView upcomingView) {
        this.upcomingView = upcomingView;
    }

    @Provides
    @PerFragment
    UpcomingFragment provideFragment() {
        return upcomingFragment;
    }
    @Provides
    @PerFragment
    UpcomingPresenter provideUpcomingPresenter(ApiSource apiSource) {
        return new UpcomingPresenter(upcomingView,apiSource);
    }
}
