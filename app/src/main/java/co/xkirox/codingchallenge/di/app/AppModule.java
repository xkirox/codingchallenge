package co.xkirox.codingchallenge.di.app;

import android.app.Application;
import android.content.Context;
import co.xkirox.codingchallenge.util.RxBus;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

/**
 * Created by xkiRox on 2/08/17.
 */

@Module
public class AppModule {
    Application application;

    public AppModule(Application application) {
        this.application = application;
    }

    @Singleton
    @Provides
    Application provideApplication() {
        return application;
    }

    @Singleton
    @Provides
    Context provideAppContext() {
        return application.getApplicationContext();
    }

    @Singleton
    @Provides
    RxBus provideRxBus(){
        return RxBus.getInstance();
    }

}
