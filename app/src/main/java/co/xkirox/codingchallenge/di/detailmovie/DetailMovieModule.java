package co.xkirox.codingchallenge.di.detailmovie;

import co.xkirox.codingchallenge.di.PerActivity;
import co.xkirox.codingchallenge.ui.detail_movie.MovieDetailActivity;
import dagger.Module;
import dagger.Provides;

/**
 * Created by xkiRox on 5/08/17.
 */

@Module
public class DetailMovieModule {

    MovieDetailActivity activity;

    @Provides
    @PerActivity
    MovieDetailActivity provideDetailMovieActivity() {
        return activity;
    }
}
