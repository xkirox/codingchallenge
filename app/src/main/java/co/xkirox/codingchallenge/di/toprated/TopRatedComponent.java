package co.xkirox.codingchallenge.di.toprated;

import co.xkirox.codingchallenge.di.PerFragment;
import co.xkirox.codingchallenge.di.app.AppComponent;
import co.xkirox.codingchallenge.ui.toprated.TopRatedFragment;
import dagger.Component;

/**
 * Created by xkiRox on 4/08/17.
 */

@PerFragment
@Component(modules = TopRatedModule.class, dependencies = AppComponent.class)
public interface TopRatedComponent {
    void inject(TopRatedFragment fragment);
}