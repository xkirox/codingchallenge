package co.xkirox.codingchallenge.di.toprated;

import co.xkirox.codingchallenge.di.PerFragment;
import co.xkirox.codingchallenge.model.api.ApiSource;
import co.xkirox.codingchallenge.mvp.presenter.toprated.TopRatedPresenter;
import co.xkirox.codingchallenge.mvp.view.toprated.ITopRatedView;
import co.xkirox.codingchallenge.ui.toprated.TopRatedFragment;
import dagger.Module;
import dagger.Provides;

/**
 * Created by xkiRox on 4/08/17.
 */


@Module
public class TopRatedModule {

    ITopRatedView topRatedView;
    TopRatedFragment fragment;

    public TopRatedModule(ITopRatedView topRatedView) {
        this.topRatedView = topRatedView;
    }

    @Provides
    @PerFragment
    TopRatedFragment provideFragment() {
        return fragment;
    }
    @Provides
    @PerFragment
    TopRatedPresenter provideTopRatedPresenter(ApiSource apiSource) {
        return new TopRatedPresenter(topRatedView,apiSource);
    }
}
