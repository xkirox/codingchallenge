package co.xkirox.codingchallenge.di.popular;

import co.xkirox.codingchallenge.di.PerFragment;
import co.xkirox.codingchallenge.model.api.ApiSource;
import co.xkirox.codingchallenge.mvp.presenter.popular.PopularPresenter;
import co.xkirox.codingchallenge.mvp.view.popular.IPopularView;
import co.xkirox.codingchallenge.ui.popular.PopularFragment;
import dagger.Module;
import dagger.Provides;

/**
 * Created by xkiRox on 4/08/17.
 */

@Module
public class PopularModule {

    IPopularView popularView;
    PopularFragment popularFragment;

    public PopularModule(IPopularView popularView) {
        this.popularView = popularView;
    }

    @Provides
    @PerFragment
    PopularFragment provideFragment() {
        return popularFragment;
    }

    @Provides
    @PerFragment
    PopularPresenter providePopularPresenter(ApiSource apiSource) {
        return new PopularPresenter(popularView,apiSource);
    }
}
